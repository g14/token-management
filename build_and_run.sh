#!/bin/bash
set -e
# build part 
mvn clean package
docker-compose build token-management 


#running part
# Cleanup the build images
docker image prune -f
docker-compose up -d rabbitMq
sleep 10s
docker-compose up -d token-management
