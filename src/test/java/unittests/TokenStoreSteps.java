package unittests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tokenmanagement.businesslogic.TokenStore;
import tokenmanagement.exceptions.CustomerNotAllowedMoreTokens;
import tokenmanagement.exceptions.InvalidNumberOfTokensException;
import tokenmanagement.exceptions.TokenNotExistingException;

/**
 * Author: Thomas Bork
 **/
public class TokenStoreSteps {
	private int customerId;
	private int amountOfTokensRequested;
	private int initialAmountOfUnusedTokens;
	private int costumerTokens;
	private String unExsistingToken;
	private String unExsistingTokenError;
	private String unusedTokenToExpire;
	private List<String> result;
	private TokenStore tokenStore;

	public TokenStoreSteps() {
		tokenStore = new TokenStore();
	}

	@After
	public void removeCustomer() {
		tokenStore.deleteCustomer(customerId);
	}

	@When("Customer Request {int} tokens with id {int}")
	public void customerRequestTokensWithId(Integer amount, Integer cid) {
		this.customerId = cid;
		this.amountOfTokensRequested = amount;
		tokenStore.addCustomerIfNotExisting(this.customerId);
	}

	@Then("We return {int} tokens")
	public void weReturnTokens(Integer int1) {
		this.result = tokenStore.createNewTokens(this.customerId, this.amountOfTokensRequested);
	}

	@Then("Each token is of type String")
	public void eachTokenIsOfTypeString() {
		assertTrue(this.result.stream().allMatch(token -> token.getClass().equals(String.class)));
	}

	@Then("Each token are {int} of length")
	public void eachTokenAreOfLength(Integer length) {
		assertTrue(this.result.stream().allMatch(token -> token.length() == length));
	}

	@Given("A customer with id {int} has {int} unused tokens")
	public void aCustomerWithIdHasUnusedTokens(Integer customerId, Integer amountOfUnusedTokens) {
		this.customerId = customerId;
		this.initialAmountOfUnusedTokens = amountOfUnusedTokens;
		tokenStore.addCustomerIfNotExisting(customerId);
		tokenStore.createNewTokens(customerId, initialAmountOfUnusedTokens);
	}

	@When("The request for {int} more tokens is being validated")
	public void theRequestForMoreTokensIsBeingValidated(Integer requestedtokens) {
		this.amountOfTokensRequested = requestedtokens;
	}

	@Then("The it throws CustomerNotAllowedMoreTokens")
	public void theItThrowsCustomerNotAllowedMoreTokens() {
		try {
			tokenStore.validateNewTokensRequest(customerId, amountOfTokensRequested);
		} catch (InvalidNumberOfTokensException e) {
			assertTrue(false);
			return;
		} catch (CustomerNotAllowedMoreTokens e) {
			assertTrue(true);
			return;
		}
		assertTrue(false);
	}

	@Then("The it throws InvalidNumberOfTokensException")
	public void theItThrowsInvalidNumberOfTokensException() {
		try {
			tokenStore.validateNewTokensRequest(customerId, amountOfTokensRequested);
		} catch (InvalidNumberOfTokensException e) {
			assertTrue(true);
			return;
		} catch (CustomerNotAllowedMoreTokens e) {
			assertTrue(false);
			return;
		}
		assertTrue(false);
	}

	@Then("Request is validated")
	public void requestIsValidated() {
		assertTrue(true);
	}

	@When("the amount of unused tokens is checked")
	public void theAmountOfUnusedTokensIsChecked() {
		costumerTokens = tokenStore.amountOfUnusedTokensByCustomer(this.customerId);
	}

	@Then("the check returns of type int and equal to {int}")
	public void theCheckReturnsOfTypeIntAndEqualTo(Integer expected) {
		assertTrue(Integer.class.isInstance(costumerTokens));
		assertEquals(expected, costumerTokens);
	}

	@When("the customer is addCustomerIfNotExisting with id {int}")
	public void theCustomerIsAddCustomerIfNotExistingWithId(Integer cid) {
		this.customerId = cid;
		tokenStore.addCustomerIfNotExisting(cid);
	}

	@Then("The costumer is added to the system")
	public void theCostumerIsAddedToTheSystem() {
		assertTrue(tokenStore.getAllTokens().keySet().contains(this.customerId));
	}

	@When("The customer is deleteCustomer")
	public void theCustomerIsDeleteCustomer() {
		tokenStore.deleteCustomer(this.customerId);
	}

	@Then("The costumer is not in the system")
	public void theCostumerIsNotInTheSystem() {
		assertTrue(!tokenStore.getAllTokens().keySet().contains(this.customerId));
	}

	@When("A unexisting customer is deleted with id {int}")
	public void aUnexistingCustomerIsDeletedWithId(Integer cid) {
		tokenStore.deleteCustomer(cid);
	}

	@When("The customer expires one of the unused tokens")
	public void theCustomerExpiresOneOfTheUnusedTokens() {
		unusedTokenToExpire = tokenStore.getAllTokens().get(this.customerId).get(0).getToken().toString();
		try {
			tokenStore.expireToken(unusedTokenToExpire);
		} catch (TokenNotExistingException e) {
			assertTrue(false);
			return;
		}
	}

	@Then("The token is expired")
	public void theTokenIsExpired() {
		assertEquals(this.initialAmountOfUnusedTokens - 1, tokenStore.amountOfUnusedTokensByCustomer(this.customerId));
		assertTrue(tokenStore.getAllTokens().get(this.customerId).get(0).isUsed());
	}

	@Given("a unexisting token {string}")
	public void aUnexistingToken(String testtoken) {
		this.unExsistingToken = testtoken;
	}

	@When("The customer expires the token")
	public void theCustomerExpiresTheToken() {
		try {
			tokenStore.expireToken(this.unusedTokenToExpire);
		} catch (TokenNotExistingException e) {
			// TODO Auto-generated catch block
			this.unExsistingTokenError = e.toString();
		}
	}

	@Then("The TokenNotExistingException is thrown")
	public void theTokenNotExistingExceptionIsThrown() {
		assertEquals("tokenmanagement.exceptions.TokenNotExistingException", this.unExsistingTokenError);
	}
}
