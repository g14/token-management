package unittests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tokenmanagement.businesslogic.TokenRepository;
import tokenmanagement.businesslogic.TokenValidator;
import tokenmanagement.model.TokenValidationResult;
import tokenmanagement.rest.TokenManagementFactory;

/**
 * Author: Rishabh Narang & Thomas Bork
 **/
public class TokenValidatorSteps {

	private int customerId;
	private int initialAmountOfTokens;
	private String oneOfTheTokens;
	private List<String> tokens;
	private TokenValidationResult result;
	private TokenRepository tokenRepo;

	public TokenValidatorSteps() {
		tokenRepo = new TokenManagementFactory().getTokenRepository();
	}

	@After
	public void removeCustomer() {
		tokenRepo.deleteCustomer(customerId);
	}

	@Given("a customer with id {int} has {int} unused token")
	public void aCustomerWithIdHasUnusedToken(Integer customerId, Integer amountOfUnusedToken) {
		this.customerId = customerId;
		this.initialAmountOfTokens = amountOfUnusedToken;
		tokenRepo.addCustomerIfNotExisting(customerId);
		tokens = tokenRepo.createNewTokens(customerId, amountOfUnusedToken);
	}

	@When("I use one of the unused tokens for validation")
	public void iUseOneOfTheUnusedTokensForValidation() {
		this.oneOfTheTokens = tokens.get(0);
	}

	@When("i call token validation method")
	public void iCallTokenValidationMethod() {
		// Write code here that turns the phrase above into concrete actions
		TokenValidator validator = new TokenValidator();
		this.result = validator.isTokenValidForPayment(oneOfTheTokens);
	}

	@Then("token validation succeeds")
	public void tokenValidationSucceeds() {
		// Write code here that turns the phrase above into concrete actions
		assertTrue(result.isValidated());
	}

	@Then("customerId returned is {int}")
	public void customerIdReturnedIs(Integer customerId) {
		// Write code here that turns the phrase above into concrete actions
		assertEquals(customerId, result.getCustomerId());
	}

	@Then("the token has been expired")
	public void theTokenHasBeenExpired() {
		boolean result = tokenRepo.getAllTokens().get(this.customerId).stream()
				.filter(token -> token.getToken().equals(this.oneOfTheTokens))
				.allMatch(token -> (token.isUsed() == true));
		assertTrue(result);
	}
}
