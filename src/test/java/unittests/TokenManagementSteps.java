package unittests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.UUID;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tokenmanagement.businesslogic.TokenManagement;
import tokenmanagement.businesslogic.TokenValidator;
import tokenmanagement.model.TokenValidationResult;
import tokenmanagement.rabbitmq.Event;
import tokenmanagement.rabbitmq.EventSender;

/**
 * Author: Rishabh Narang
 **/

public class TokenManagementSteps {

	private TokenManagement tokenManagementService;
	Event sentEvent;
	private TokenValidator validator;

	public TokenManagementSteps() {
		this.validator = mock(TokenValidator.class);
		tokenManagementService = new TokenManagement(new EventSender() {
			@Override
			public void sendEvent(Event e) throws Exception {
				sentEvent = e;
			}
		}, this.validator);
	}

	@When("I receive event {string}")
	public void iReceiveEvent(String event) throws Exception {
		String testToken = UUID.randomUUID().toString();
		when(this.validator.isTokenValidForPayment(testToken))
				.thenReturn(new TokenValidationResult(1, testToken, false));
		Event testEvent = new Event(event, new Object[] { testToken });
		tokenManagementService.receiveEvent(testEvent);
	}

	@Then("I send event {string}")
	public void iSendEvent(String sentEvent) {
		// Write code here that turns the phrase above into concrete actions
		assertEquals(sentEvent, this.sentEvent.getEventType());
	}

}
