package servicetests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tokenmanagement.businesslogic.TokenManagement;
import tokenmanagement.businesslogic.TokenRepository;
import tokenmanagement.exceptions.CustomerNotAllowedMoreTokens;
import tokenmanagement.exceptions.InvalidNumberOfTokensException;
import tokenmanagement.model.Token;
import tokenmanagement.rest.TokenManagementFactory;

/**
 * Author: Rishabh Narang & Thomas Bork
 **/
public class TokenCreationSteps {
	private TokenManagement tokenManagementService;
	private TokenRepository tokenRepo;
	private int customerId;
	private int initialAmountOfUnusedTokens;
	private int amountOfTokensRequested;
	private boolean unique = true;

	public TokenCreationSteps() {
		tokenManagementService = new TokenManagement();
		tokenRepo = new TokenManagementFactory().getTokenRepository();
	}

	@After
	public void removeCustomer() {
		tokenRepo.deleteCustomer(customerId);
	}

	@Given("a customer with id {int} with {int} unused tokens")
	public void aCustomerWithIdWithUnusedTokens(Integer customerId, Integer amountOfUnusedTokens)
			throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens {
		this.customerId = customerId;
		this.initialAmountOfUnusedTokens = amountOfUnusedTokens;
		tokenRepo.addCustomerIfNotExisting(customerId);
		tokenManagementService.generateTokens(customerId, amountOfUnusedTokens);
	}

	@When("the customer requests {int} tokens")
	public void theCustomerRequestsTokens(Integer amount) {
		this.amountOfTokensRequested = amount;
	}

	@Then("I generate {int} new tokens for the customer")
	public void iGenerateNewTokensForTheCustomer(Integer amountOfNewTokensCreated)
			throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens {
		tokenManagementService.generateTokens(customerId, amountOfNewTokensCreated);
		assertTrue(tokenRepo.amountOfUnusedTokensByCustomer(this.customerId) > initialAmountOfUnusedTokens);
	}

	@Then("total unused number of tokens for the customer is {int}")
	public void totalUnusedNumberOfTokensForTheCustomerIs(Integer finalAmount) {
		assertEquals(finalAmount, tokenRepo.amountOfUnusedTokensByCustomer(customerId));
	}

	@Then("customer request is declined")
	public void customerRequestIsDeclined() {
		try {
			tokenManagementService.generateTokens(customerId, amountOfTokensRequested);
		} catch (CustomerNotAllowedMoreTokens | InvalidNumberOfTokensException e) {
			assertTrue(true);
			return;
		}
		assertTrue(false);
	}

	@When("I Generate {int} tokens with the customer id {int}")
	public void iGenerateTokensWithTheCustomerId(Integer amount, Integer cid) {
		this.amountOfTokensRequested = amount;
		this.customerId = cid;
	}

	@Then("I generate x unique token")
	public void iGenerateXUniqueToken() throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens {
		tokenRepo.addCustomerIfNotExisting(this.customerId);
		List<String> generatedTokens = tokenRepo.createNewTokens(this.customerId, this.amountOfTokensRequested);
		assertTrue(checkUniqueness(generatedTokens));
	}

	@Then("validation is successful of generated token")
	public void validationIsSuccessfulOfGeneratedToken() {
		List<Token> customerTokens = tokenRepo.getAllTokens().get(this.customerId);
		assertTrue(
				checkUniqueness(customerTokens.stream().map(token -> token.getToken()).collect(Collectors.toList())));
	}

	private boolean checkUniqueness(List<String> generatedTokens) {
		// Get the stream from the list
		Set<String> duplicateTokens = generatedTokens.stream()
				// Count the frequency of each element
				// and filter the elements
				// with frequency > 1
				.filter(i -> Collections.frequency(generatedTokens, i) > 1)
				// And Collect them in a Set
				.collect(Collectors.toSet());
		return duplicateTokens.isEmpty();
	}

}
