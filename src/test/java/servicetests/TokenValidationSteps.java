package servicetests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tokenmanagement.businesslogic.TokenManagement;
import tokenmanagement.businesslogic.TokenRepository;
import tokenmanagement.businesslogic.TokenValidator;
import tokenmanagement.exceptions.CustomerNotAllowedMoreTokens;
import tokenmanagement.exceptions.InvalidNumberOfTokensException;
import tokenmanagement.exceptions.TokenNotExistingException;
import tokenmanagement.rabbitmq.Event;
import tokenmanagement.rabbitmq.EventSender;
import tokenmanagement.rest.TokenManagementFactory;

public class TokenValidationSteps {

	private TokenManagement tokenManagementService;
	private TokenRepository tokenRepo;
	private String createdToken;
	private Event sentEvent;
	private int customerId;

	public TokenValidationSteps() {
		TokenValidator validator = new TokenValidator();
		tokenManagementService = new TokenManagement(new EventSender() {
			@Override
			public void sendEvent(Event e) throws Exception {
				sentEvent = e;
			}
		}, validator);
		tokenRepo = new TokenManagementFactory().getTokenRepository();
	}

	@After
	public void removeCustomer() {
		tokenRepo.deleteCustomer(customerId);
	}

	@Given("a customer with id {int} with an unused token exists")
	public void aCustomerWithIdWithAnUnusedTokenExists(Integer customerId)
			throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens {
		// Write code here that turns the phrase above into concrete actions
		this.customerId = customerId;
		tokenRepo.addCustomerIfNotExisting(customerId);
		createdToken = tokenRepo.createNewTokens(customerId, 1).get(0);
	}

	@When("I receive event {string} with the existing token")
	public void iReceiveEventWithTheExistingToken(String event) throws Exception {
		tokenManagementService.receiveEvent(new Event(event, new Object[] { createdToken }));
	}

	@Then("validation is successful")
	public void validationIsSuccessful() {
		assertTrue((boolean) sentEvent.getArguments()[0]);
	}

	@Then("i send event {string}")
	public void iSendEvent(String sentEventName) {
		assertEquals(sentEventName, sentEvent.getEventType());
	}

	@Given("a customer with id {int} with a used token exists")
	public void aCustomerWithIdWithAUsedTokenExists(Integer customerId)
			throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens, TokenNotExistingException {
		this.customerId = customerId;
		tokenRepo.addCustomerIfNotExisting(customerId);
		createdToken = tokenRepo.createNewTokens(customerId, 1).get(0);
		tokenRepo.expireToken(createdToken);
	}

	@Then("validation is unsuccessful")
	public void validationIsUnsuccessful() {
		assertFalse((boolean) sentEvent.getArguments()[0]);
	}

	@Given("a customer with id {int} never having received a token {string}")
	public void aCustomerWithIdNeverHavingReceivedAToken(Integer customerId, String testoken) {
		this.customerId = customerId;
		tokenRepo.addCustomerIfNotExisting(customerId);
		this.createdToken = testoken;
	}

	@When("I receive event {string} with token {string}")
	public void iReceiveEventWithToken(String event, String token) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		tokenManagementService.receiveEvent(new Event(event, new Object[] { token }));
	}

	@Then("we send the same token back")
	public void weSendTheSameTokenBack() {
		assertEquals(this.createdToken, sentEvent.getArguments()[2]);
	}

}
