#Author: Rishabh Narang

Feature: Token validator

  Scenario: Successful token validation
    Given a customer with id 1 has 2 unused token
    When I use one of the unused tokens for validation
    And i call token validation method
    Then token validation succeeds
    And customerId returned is 1
    And the token has been expired

