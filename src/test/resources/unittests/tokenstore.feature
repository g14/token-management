#Author: Thomas Bork
Feature: Token Store

	Scenario: createNewTokens
		When Customer Request 5 tokens with id 1
		Then We return 5 tokens
		And Each token is of type String
		And Each token are 36 of length

	Scenario: validateNewTokensRequest throws CustomerNotAllowedMoreTokens
		Given A customer with id 1 has 2 unused tokens
		When The request for 5 more tokens is being validated
		Then The it throws CustomerNotAllowedMoreTokens
	
	Scenario: validateNewTokensRequest throws InvalidNumberOfTokensException
		Given A customer with id 1 has 2 unused tokens
		When The request for 6 more tokens is being validated
		Then The it throws InvalidNumberOfTokensException
		
	Scenario: validateNewTokensRequest is successful
		Given A customer with id 1 has 2 unused tokens
		When The request for 3 more tokens is being validated
		Then Request is validated
		
	Scenario: amountOfUnusedTokensByCustomer is checked with customer
		Given A customer with id 1 has 2 unused tokens
		When the amount of unused tokens is checked
		Then the check returns of type int and equal to 2
		
		
	Scenario: addCustomerIfNotExisting with unexisting customer
		When the customer is addCustomerIfNotExisting with id 1
		Then The costumer is added to the system
	
	Scenario: deleteCustomer with existing customer
		Given A customer with id 1 has 2 unused tokens
		When The customer is deleteCustomer
		Then The costumer is not in the system
	
	Scenario: expireToken with existing token
		Given A customer with id 1 has 2 unused tokens
		When The customer expires one of the unused tokens
		Then The token is expired
		
	Scenario: expireToken with unexisting token
		Given a unexisting token "test-token"
		When The customer expires the token
		Then The TokenNotExistingException is thrown
