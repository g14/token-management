#Author: Rishabh Narang & Thomas Bork
Feature: Token validation
	
	Scenario: Validation of an unused token
		Given a customer with id 1 with an unused token exists
		When I receive event "TokenToBeValidatedQueue" with the existing token
		Then validation is successful
		And i send event "TokenValidatedQueue"
		And we send the same token back
	
	Scenario: Validation of a used token
		Given a customer with id 2 with a used token exists
		When I receive event "TokenToBeValidatedQueue" with the existing token
		Then validation is unsuccessful
		And i send event "TokenValidatedQueue"
		And we send the same token back
	
	Scenario: Validation of a non-existing token
		Given a customer with id 3 never having received a token "test-token"
		When I receive event "TokenToBeValidatedQueue" with token "test-token"
		Then validation is unsuccessful
		And i send event "TokenValidatedQueue"
		And we send the same token back
		
