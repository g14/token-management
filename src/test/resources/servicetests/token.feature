#Author: Rishabh Narang & Thomas Bork
Feature: Token Creation

  Scenario: Successful token request
  	Given a customer with id 1 with 1 unused tokens
  	When the customer requests 3 tokens
  	Then  I generate 3 new tokens for the customer
  	And total unused number of tokens for the customer is 4
  	
  Scenario: Successful token request
  	Given a customer with id 4 with 1 unused tokens
  	When the customer requests 4 tokens
  	Then  I generate 4 new tokens for the customer
  	And total unused number of tokens for the customer is 5
  	
  Scenario: Unsuccessful token request
  	Given a customer with id 2 with 2 unused tokens
  	When the customer requests 5 tokens
  	Then customer request is declined
  	And total unused number of tokens for the customer is 2
  	
  Scenario: Unsuccessful token request
  	Given a customer with id 3 with 3 unused tokens
  	When the customer requests -2 tokens
  	Then customer request is declined
  	And total unused number of tokens for the customer is 3
  	
  Scenario: one unique token
  	When I Generate 5 tokens with the customer id 1
  	Then I generate x unique token
  	Then validation is successful of generated token