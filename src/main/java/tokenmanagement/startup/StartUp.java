package tokenmanagement.startup;

import org.apache.log4j.BasicConfigurator;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import tokenmanagement.businesslogic.TokenManagement;
import tokenmanagement.businesslogic.TokenValidator;
import tokenmanagement.rabbitmq.EventSender;
import tokenmanagement.rabbitmq.RabbitMqListener;
import tokenmanagement.rabbitmq.RabbitMqSender;
/** 
 * Author: Rishabh Narang
 * **/
@QuarkusMain
public class StartUp implements QuarkusApplication {
	public static void main(String[] args) throws Exception {
		Quarkus.run(StartUp.class, args);
	}

	private void startUp() throws Exception {
		EventSender s = new RabbitMqSender();
		TokenValidator validator = new TokenValidator();
		TokenManagement service = new TokenManagement(s, validator);
		BasicConfigurator.configure();
		new RabbitMqListener(service).listen();
	}

	@Override
	public int run(String... args) throws Exception {
		startUp();
		Quarkus.waitForExit();
		return 0;
	}
}