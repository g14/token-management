package tokenmanagement.businesslogic;

import java.util.List;

import org.apache.log4j.Logger;

import tokenmanagement.exceptions.CustomerNotAllowedMoreTokens;
import tokenmanagement.exceptions.InvalidNumberOfTokensException;
import tokenmanagement.model.TokenValidationResult;
import tokenmanagement.rabbitmq.Event;
import tokenmanagement.rabbitmq.EventReceiver;
import tokenmanagement.rabbitmq.EventSender;
import tokenmanagement.rest.TokenManagementFactory;

public class TokenManagement implements EventReceiver {

	private TokenValidator validator;
	private EventSender eventSender;
	private static final String VALIDATE_TOKEN_EVENT_NAME = "TokenToBeValidatedQueue";
	private static final String VALIDATE_TOKEN_RESULT_EVENT_NAME = "TokenValidatedQueue";
	private static final Logger logger = Logger.getLogger(TokenManagement.class);

	public TokenManagement(EventSender eventSender, TokenValidator validator) {
		this.eventSender = eventSender;
		this.validator = validator;
	}

	public TokenManagement() {
	}

	/**
	 * Author: Rishabh Narang
	 **/
	@Override
	public void receiveEvent(Event event) throws Exception {
		// which event to listen and handle
		if (event.getEventType().equals(VALIDATE_TOKEN_EVENT_NAME)) {
			handleValidationEvent(event);
		} else {
			logger.info("event ignored: " + event);
		}
	}

	private void handleValidationEvent(Event event) throws Exception {

		logger.info("event handled: " + event);
		TokenValidationResult result = this.validator.isTokenValidForPayment((String) event.getArguments()[0]);
		Event tokenProducedEvent = new Event(VALIDATE_TOKEN_RESULT_EVENT_NAME,
				new Object[] { result.isValidated(), result.getCustomerId(), (String) event.getArguments()[0] });
		eventSender.sendEvent(tokenProducedEvent);

	}

	/**
	 * Author: Thomas Bork
	 **/
	public List<String> generateTokens(int customerId, int amount)
			throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens {
		// Exception thrown if request is invalid
		TokenRepository tokenRepo = new TokenManagementFactory().getTokenRepository();
		tokenRepo.validateNewTokensRequest(customerId, amount);
		logger.info("INFO: token creation request can be successfully served.");
		return tokenRepo.createNewTokens(customerId, amount);
	}

}
