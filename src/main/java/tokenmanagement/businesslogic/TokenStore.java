package tokenmanagement.businesslogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import tokenmanagement.exceptions.CustomerNotAllowedMoreTokens;
import tokenmanagement.exceptions.InvalidNumberOfTokensException;
import tokenmanagement.exceptions.TokenNotExistingException;
import tokenmanagement.model.Token;
import tokenmanagement.model.TokenValidationResult;

/*
 * Author: Rishabh Narang
 * Use this class as sort of in-memory database. Stores the list of tokens generated in
 * the past
 */

public class TokenStore implements TokenRepository {

	private static HashMap<Integer, List<Token>> allTokens = new HashMap<>();
	private static HashMap<String, Integer> tokenToCustomerMap = new HashMap<>();
	private static final Logger logger = Logger.getLogger(TokenStore.class);

	public HashMap<Integer, List<Token>> getAllTokens() {
		return allTokens;
	}

	public List<String> createNewTokens(int customerId, int numTokensToAdd) {
		// create new tokens
		List<String> newTokens = new ArrayList<String>(numTokensToAdd);
		List<Token> newTokenObjects = new ArrayList<Token>(numTokensToAdd);
		for (int i = 0; i < numTokensToAdd; i++) {
			String newToken = UUID.randomUUID().toString();
			newTokens.add(newToken);
			newTokenObjects.add(new Token(newToken, false));
			tokenToCustomerMap.put(newToken, customerId);
		}

		// Store the tokens corresponding to the customer
		allTokens.get(customerId).addAll(newTokenObjects);
		logger.info(" tokens created are:" + newTokens.toString());
		return newTokens;
	}

	public void validateNewTokensRequest(int customerId, int numTokensToAdd)
			throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens {
		if (!isTokenAmountValid(numTokensToAdd)) {
			throw new InvalidNumberOfTokensException();
		} else if (!isCustomerAllowedMoreTokens(customerId)) {
			throw new CustomerNotAllowedMoreTokens();
		}
	}

	private boolean isTokenAmountValid(int numTokensToAdd) {
		return (numTokensToAdd < 6 && numTokensToAdd > 0);
	}

	private boolean isCustomerAllowedMoreTokens(int customerId) {
		long cntUnusedTokens = amountOfUnusedTokensByCustomer(customerId);
		return cntUnusedTokens < 2;
	}

	public int amountOfUnusedTokensByCustomer(int customerId) {
		List<Token> customerTokens = allTokens.get(customerId);
		// TODO remove this in future?
		if (customerTokens == null) {
			addCustomerIfNotExisting(customerId);
			customerTokens = new ArrayList<>();
		}
		return (int) customerTokens.stream().filter(token -> !token.isUsed()).count();
	}

	public void addCustomerIfNotExisting(int customerId) {
		if (allTokens.get(customerId) == null) {
			allTokens.put(customerId, new ArrayList<Token>());
		}
	}

	public void deleteCustomer(int customerId) {
		allTokens.remove(customerId);
	}

	public TokenValidationResult expireToken(String token) throws TokenNotExistingException {
		logger.info("Token validation starts...");
		int customerId = getCustomerId(token);
		List<Token> tokens = allTokens.get(customerId);
		boolean isValidated = false;

		for (int i = 0; i < tokens.size(); i++) {
			if (tokens.get(i).getToken().equals(token)) {
				boolean initalTokenStatus = tokens.get(i).isUsed();
				tokens.get(i).setUsed(true);
				isValidated = !initalTokenStatus;
			}
		}
		logger.info("finished token validation = " + isValidated);
		return new TokenValidationResult(customerId, token, isValidated);

	}

	private int getCustomerId(String token) throws TokenNotExistingException {
		Integer customerId = tokenToCustomerMap.get(token);
		if (customerId != null)
			return customerId;
		throw new TokenNotExistingException();
	}

}
