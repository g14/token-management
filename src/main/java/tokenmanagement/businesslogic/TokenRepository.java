package tokenmanagement.businesslogic;

import java.util.HashMap;
import java.util.List;

import tokenmanagement.exceptions.CustomerNotAllowedMoreTokens;
import tokenmanagement.exceptions.InvalidNumberOfTokensException;
import tokenmanagement.exceptions.TokenNotExistingException;
import tokenmanagement.model.Token;
import tokenmanagement.model.TokenValidationResult;

/**
 * Author: Rishabh Narang
 **/
public interface TokenRepository {

	public List<String> createNewTokens(int customerId, int numTokensToAdd);

	public HashMap<Integer, List<Token>> getAllTokens();

	public void validateNewTokensRequest(int customerId, int numTokensToAdd)
			throws InvalidNumberOfTokensException, CustomerNotAllowedMoreTokens;

	public int amountOfUnusedTokensByCustomer(int customerId);

	public void addCustomerIfNotExisting(int customerId);

	public void deleteCustomer(int customerId);

	public TokenValidationResult expireToken(String token) throws TokenNotExistingException;

}
