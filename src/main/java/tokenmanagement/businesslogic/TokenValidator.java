package tokenmanagement.businesslogic;

import tokenmanagement.exceptions.TokenNotExistingException;
import tokenmanagement.model.TokenValidationResult;
import tokenmanagement.rest.TokenManagementFactory;

/*
 * Author : Rishabh Narang
 * TokenValidator class is used to validate the tokens used for transferring money in DtuPay
 */
public class TokenValidator {

	public TokenValidationResult isTokenValidForPayment(String token) {
		try {
			TokenRepository tokenRepo = new TokenManagementFactory().getTokenRepository();
			return tokenRepo.expireToken(token);
		} catch (TokenNotExistingException e) {
			return new TokenValidationResult(-1, token, false);
		}
	}

}
