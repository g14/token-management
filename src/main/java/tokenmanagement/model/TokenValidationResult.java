package tokenmanagement.model;

/**
 * Author: Rishabh Narang
 **/
public class TokenValidationResult {

	private int customerId;
	private String token;
	private boolean isValidated;

	public TokenValidationResult(int customerId, String token, boolean isValidated) {
		this.customerId = customerId;
		this.token = token;
		this.isValidated = isValidated;
	}

	public int getCustomerId() {
		return customerId;
	}

	public boolean isValidated() {
		return isValidated;
	}

}
