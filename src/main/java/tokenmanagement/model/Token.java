package tokenmanagement.model;
/** 
 * Author: Thomas Bork
 * **/
public class Token {

	private String token;
	private boolean isUsed;
	
	public Token(String token, boolean isUsed) {
		this.token = token;
		this.isUsed = isUsed;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isUsed() {
		return isUsed;
	}

	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}
	
	
}
