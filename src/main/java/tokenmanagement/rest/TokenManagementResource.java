package tokenmanagement.rest;

import java.util.List;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import tokenmanagement.businesslogic.TokenManagement;
import tokenmanagement.exceptions.CustomerNotAllowedMoreTokens;
import tokenmanagement.exceptions.InvalidNumberOfTokensException;

/**
 * Author: Thomas Bork
 **/
@Path("/token")
public class TokenManagementResource {

	private static final Logger logger = Logger.getLogger(TokenManagementResource.class);

	@GET
	@Path("/{customerId}/{amount}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> generateTokens(@PathParam("customerId") int customerId, @PathParam("amount") int amount) {
		TokenManagement service = new TokenManagementFactory().getService();
		try {
			logger.info(String.format("INFO: Received rest request with customerId= %s and amount= %d ", customerId,
					amount));
			return service.generateTokens(customerId, amount);
		} catch (InvalidNumberOfTokensException e) {
			throw new ForbiddenException("Number of tokens requested can only be between 1 and 5, inclusive.");
		} catch (CustomerNotAllowedMoreTokens e) {
			throw new ForbiddenException(
					"Customer can't be given more tokens until he/she has atmost one unused token left.");
		}

	}

}
