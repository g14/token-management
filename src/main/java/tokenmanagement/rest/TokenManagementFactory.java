package tokenmanagement.rest;

import tokenmanagement.businesslogic.TokenManagement;
import tokenmanagement.businesslogic.TokenRepository;
import tokenmanagement.businesslogic.TokenStore;
import tokenmanagement.businesslogic.TokenValidator;
import tokenmanagement.rabbitmq.EventSender;
import tokenmanagement.rabbitmq.RabbitMqSender;

public class TokenManagementFactory {
	private static TokenManagement service = null;
	private static TokenRepository repository = null;

	public TokenManagement getService() {

		if (service != null) {
			return service;
		}

		EventSender tokensender = new RabbitMqSender();
		TokenValidator validator = new TokenValidator();

		service = new TokenManagement(tokensender, validator);
		return service;
	}

	public TokenRepository getTokenRepository() {
		if (repository != null) {
			return repository;
		}
		repository = new TokenStore();
		return repository;

	}

}
