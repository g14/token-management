package tokenmanagement.rabbitmq;

public interface EventReceiver {
	void receiveEvent(Event event) throws Exception;
}
