package tokenmanagement.rabbitmq;

public interface EventSender {

	void sendEvent(Event event) throws Exception;

}
